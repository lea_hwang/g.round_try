package jpabook.jpashop;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class HelloController {

    @GetMapping("hello")
    public String hello(Model model){
        model.addAttribute("data", "hello!!!"); // 키 값
        return "hello"; // 위치 : main > resources > templates > hello.html(View 화면 렌더링 페이지)
    }

}
